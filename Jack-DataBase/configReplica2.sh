#!/bin/bash


# VARIABLES 

resourceGroup="OCC_ASD_Ayoub"
Bdd1Name="myfirstmysqldb"
location="francecentral"
myVnet="equipe4_Vnet"
subReplica="equipe4_Vnet_Subnet_database"
dns="myfirstmysqldb.private.mysql.database.azure.com"


az mysql flexible-server replica create \
    --resource-group $resourceGroup \
    --replica-name replicaserver \
    --source-server $Bdd1Name \
    --location $location \
    --vnet $myVnet \
    --subnet $subReplica \
    --private-dns-zone $dns 
