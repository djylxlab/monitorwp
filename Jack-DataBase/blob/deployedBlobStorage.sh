#!/bin/bash

#Deploying an Azure Blob Storage
echo "Deploying an Azure Blob Storage with a Storage account and a Blob Container"
ansible-playbook playbookBlob.yml

#Configure the server for using wp cli"
echo "Install on your wordpress server wp cli and add the user to the good group"
ansible-playbook -u ayoub playbookBlob2.yml

echo "Take a break ! I restart the servers for 2 minutes"
sleep 120

#Configure the plugin for using Blob Storage"
echo "Install more packages and configure the wordpress's plugin for using with Blob."
ansible-playbook -u ayoub playbookBlob3.yml

echo "Take an other break for 5 minutes. During that, copy your Storage account Name and Key on your plugin's parameters on your wordpress's website"
sleep 300
echo "It's Done !!"

#Configure an Azure function"
#echo "Take in place an Azure function for send the image's URL in blob to Azure Database for mysql"
#ansible-playbook playbookBlob4.yml
