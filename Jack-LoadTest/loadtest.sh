#!/bin/bash

# Variables

loadTestResource="wpLoadTest"
resourceGroup="OCC_ASD_JACQUES"
location="francecentral"
testId="sample-test-wp"
testPlan="sample.jmx"
testRunId="run_"`date +"%Y%m%d%_H%M%S"`
displayName="Run"`date +"%Y/%m/%d_%H:%M:%S"`



#    --test-plan $testPlan \
#Create a load test preview

az load create \
    --name $loadTestResource \
    --resource-group $resourceGroup \
    --location $location 


az load test create \
    --resource-group $resourceGroup \
    --test-id $testId \
    --load-test-resource $loadTestResource \
    --display-name "My ClI Load Test" \
    --description "Create a load test" \
    --env webapp="20.188.42.122/wordpress/" rps=100000 \
    --engine-instances 1


az load test-run create \
    --resource-group $resourceGroup \
    --load-test-resource $loadTestResource \
    --test-id $testId \
    --test-run-id $testRunId \
    --display-name $displayName \
    --description "Test run"


az load test-run metrics list \
    --resource-group $resourceGroup \
    --load-test-resource $loadTestResource \
    --test-run-id $testRunId \
    --metric-namespace LoadTestRunMetrics
