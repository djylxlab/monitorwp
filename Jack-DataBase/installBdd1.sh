#!/bin/bash


# VARIABLES 

resourceGroup="OCC_ASD_Ayoub"
Bdd1Name="myfirstmysqldb"
location="francecentral"
adminName="mysqlAdmin"
adminPasswd="admin120686!"
myVnet="equipe4_Vnet"
subDb="equipe4_Vnet_Subnet_database"

# CREATE A SUBNET FOR AZURE DATABASE FOR MYSQL 

az network vnet subnet create \
    --name $subDb \
    --resource-group $resourceGroup \
    --vnet-name $myVnet \
    --address-prefixes 10.0.10.96/28 
#    --disable-private-endpoint-network-policies false

# CREATE A FIRST AZURE DATABASE FOR MYSQL

az mysql flexible-server create \
    --resource-group $resourceGroup \
    --name $Bdd1Name \
    --location $location \
    --admin-user $adminName \
    --admin-password $adminPasswd \
    --sku-name Standard_D2ds_v4 \
    --tier GeneralPurpose \
    --storage-size 32 \
    --version 8.0.21 \
    --vnet $myVnet \
    --subnet $subDb \
    --database-name wordpress \
    --yes
