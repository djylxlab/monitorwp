# Secure WordPress Deployment on Azure

This repository contains infrastructure as code (IaC) scripts and configurations for deploying a highly secure WordPress website on Microsoft Azure. The deployment emphasizes robust networking practices and incorporates key security measures to safeguard the WordPress environment.

## Deployment Overview

The deployment architecture consists of meticulously designed components to ensure the security, scalability, and performance of the WordPress site:

1. **Firewall**:
   - **Purpose**: Acts as the first line of defense by restricting incoming traffic to essential ports (80 for HTTP and 443 for HTTPS).
   - **Configuration**: IP restrictions enforced to limit access to authorized IP addresses only.
   - **Associated Script**: `equipe4_infra.yml`

2. **Internal Load Balancer (ILB)**:
   - **Purpose**: Distributes incoming web traffic across multiple WordPress servers for load balancing and fault tolerance.
   - **Configuration**: Configured with a private IP address to ensure internal network traffic remains secure.
   - **Associated Script**: `equipe4_infra.yml`

3. **WordPress Web Servers** (2 instances):
   - **Configuration**: WordPress installed and hardened on each server, incorporating security best practices.
   - **Monitoring**: Application Insights plugin deployed for comprehensive Azure monitoring and performance analysis.
   - **Associated Script**: `equipe4_configWP.yml`

4. **MySQL SaaS Database**:
   - **Configuration**: Managed MySQL database service utilized for data storage, ensuring high availability and data integrity.
   - **Storage**: Blob disk employed for efficiently storing and serving images and media files.
   - **Connectivity**: Encrypted connections established from WordPress servers to the MySQL database for enhanced security.
   - **Associated Script**: Not applicable (configuration managed through Azure portal or other means).

5. **Bastion Host**:
   - **Purpose**: Facilitates secure administrative access to WordPress servers by providing a controlled and monitored entry point.
   - **Configuration**: Public IP address assigned to the bastion host for secure SSH access.
   - **Secure Access**: SSH port (22) closed on the firewall to mitigate unauthorized access attempts.
   - **Associated Script**: Not applicable (configuration managed through Azure portal or other means).

6. **NAT Gateway**:
   - **Purpose**: Enables outbound internet connectivity for WordPress servers while alleviating load on the ILB.
   - **Load Balancer Bypass**: Direct outbound traffic routing via NAT gateway to prevent ILB overload and optimize network performance.
   - **Associated Script**: `equipe4_infra.yml`

## Deployment Scripts and Automation

The deployment process is streamlined and automated through a combination of Ansible playbooks and shell scripts, leveraging the Azure CLI for infrastructure provisioning and configuration management.

- **`equipe4_WP.sh`**: Main script that calls the different playbooks. It uses some az commands to create the Bastion and updating the load balancer. 
- **`equipe4_infra.yml`**: Ansible playbook to create the Vnet, subnets, firewall, VMs, NICs, and NSG.
- **`equipe4_configWP.yml`**: Ansible playbook to configure WordPress.
- **`insights.plugin.yml`**: Ansible playbook for installing and activating Application Insights plugin.
- **`Jack-DataBase/playbook.yml`**: Ansible playbbok which calls two scripts to install flexible server and its replica. The first bash script installs a Subnet delegate to the flexible server and installs it. The second script, installs the replication of the first mysql flexible server for the high availabality and auto `fail-over`.
- **`Jack-DataBase/blob/deployedBlobStorage.sh`**: Bash script to install and configure the Azure Blob Storage for images hosting. It calls three ansible playbooks. The first, creates a storage account and the blob container. The second, the `wp cli` and user's parameters. The third playbbok installs the wordress plugin `windows-azure-storage` on the two servers.

## Usage

1. **Clone Repository**: Clone this repository to your local machine.
2. **Azure CLI**: Ensure the Azure CLI is installed and authenticated with appropriate permissions.
3. **Run Deployment Script**: Execute the `deploy.sh` script to initiate the deployment process and follow the prompts for configuration inputs.
4. **Load Test**: You can access the different load tests here : https://portal.azure.com/#view/Microsoft_Azure_CloudNativeTesting/NewReport/testRunId/09b71713-14db-41f6-b860-8ac995f2b24e/testId/e088f3c9-6b7a-4aef-bd8f-4a89b831c21e/resourceId/%2Fsubscriptions%2F0b15b40d-18c9-4474-8fa0-26b43d03ba8c%2Fresourcegroups%2Focc_asd_jacques%2Fproviders%2Fmicrosoft.loadtestservice%2Floadtests%2Fwploadtest/openingFromTestRunBlade~/true
5. **Website URL**: For the exercice, you can consult our website in two ways. The first way uses ip address of the firewall (example: http://4.212.11.72/wordpress/). The second, you can use a domain name : http://asdramteam4.ddns.net/wordpress/ (this way will not sustain)

## Contributors 
1. Jacques GARCIA aka Djylx
2. Ayoub FNIZI aka PO



