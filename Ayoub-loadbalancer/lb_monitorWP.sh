#!/bin/bash

ansible-playbook ayoub_lb.yml

sleep 1

#Attach an inbound nat rule to an existing NIC
az network nic ip-config inbound-nat-rule add \
    -g OCC_ASD_Ayoub \
    --nic-name NIC1 \
    --ip-config-name default \
    --lb-name WP-loadbalancer \
    --inbound-nat-rule inboundnatrule1



az network nic ip-config inbound-nat-rule add \
    -g OCC_ASD_Ayoub \
    --nic-name NIC2 \
    --ip-config-name default \
    --lb-name WP-loadbalancer \
    --inbound-nat-rule inboundnatrule2


# Get the list of VM IP addresses in the whole resource group
ip_addresses=$(az vm list-ip-addresses -g OCC_ASD_Ayoub --query "[].virtualMachine.network.privateIpAddresses[0]" -o tsv)

# Loop through each IP address and add it to the backend pool
echo "Populating backend pool with IP addresses:"

for ip in $ip_addresses; do
    echo "Adding $ip to the backend pool"
    az network lb address-pool address add \
    -g OCC_ASD_Ayoub \
    --lb-name WP-loadbalancer \
    --pool-name backendpool \
    -n "$ip" \
    --subnet equipe4_Vnet_Subnet_webservers \
    --vnet equipe4_Vnet \
    --ip-address "$ip"
done

sleep 3

cd /home/ayoub/monitorwp/Ayoub-loadbalancer/wordpress 
ansible-playbook playbook.yml 
