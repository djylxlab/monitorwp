#!/bin/bash

echo "Executing playbook to create a vnet, the 3 subnets, the firewall, the VMs, the NICs and the NSG"
ansible-playbook equipe4_infra.yml

echo "sleeping 30 seconds"
sleep 30

#Displaying what is about to happen before executing the command
echo "Creating public IP address for Bastion..."
az network public-ip create --resource-group OCC_ASD_Ayoub --name myBastionIP --sku Standard

echo "Creating subnet for Azure Bastion..."
az network vnet subnet create --resource-group OCC_ASD_Ayoub --name AzureBastionSubnet --vnet-name equipe4_Vnet --address-prefixes 10.0.10.80/28

echo "Creating Azure Bastion..."
az network bastion create --resource-group OCC_ASD_Ayoub --name myBastionHost --public-ip-address myBastionIP --vnet-name equipe4_Vnet --location francecentral

echo "Creating Load Balancer..."
az network lb create --resource-group OCC_ASD_Ayoub --name myLoadBalancer --sku Standard --vnet-name equipe4_Vnet --subnet equipe4_Vnet_Subnet_webservers --backend-pool-name myBackEndPool --frontend-ip-name myFrontEnd

echo "Creating health probe for Load Balancer..."
az network lb probe create --resource-group OCC_ASD_Ayoub --lb-name myLoadBalancer --name myHealthProbe --protocol tcp --port 80

echo "Creating rule for Load Balancer..."
az network lb rule create --resource-group OCC_ASD_Ayoub --lb-name myLoadBalancer --name myHTTPRule --protocol tcp --frontend-port 80 --backend-port 80 --frontend-ip-name myFrontEnd --backend-pool-name myBackEndPool --probe-name myHealthProbe --idle-timeout 15 --enable-tcp-reset true

#echo "Creating Network Security Group..."
#az network nsg create --resource-group OCC_ASD_Ayoub --name myNSG

#echo "Creating rule for Network Security Group..."
#az network nsg rule create --resource-group OCC_ASD_Ayoub --nsg-name myNSG --name myNSGRuleHTTP --protocol '*' --direction inbound --source-address-prefix '*' --source-port-range '*' --destination-address-prefix '*' --destination-port-range 80 --access allow --priority 200

# Create network interfaces for virtual machines
#array=(myNicVM1 myNicVM2)
#for vmnic in "${array[@]}"
#do
#  echo "Creating network interface for VM: $vmnic..."
#  az network nic create --resource-group OCC_ASD_Ayoub --name $vmnic --vnet-name equipe4_Vnet --subnet equipe4_Vnet_Subnet_webservers --network-security-group myNSG
#done

# Create virtual machines
#array=(1 2)
#for n in "${array[@]}"
#do
#  echo "Creating virtual machine: myVM$n..."
#  az vm create --resource-group OCC_ASD_Ayoub --name myVM$n --nics myNicVM$n --image SuseSles15SP3 --admin-username azureuser --zone $n --no-wait
#done

# Assign IP addresses from the load balancer backend pool to NICs
array=(1 2)
for item in "${array[@]}"
do
  echo "Assigning IP address from Load Balancer backend pool to NIC: NIC$item..."
  az network nic ip-config address-pool add --address-pool myBackendPool --ip-config-name default --nic-name NIC$item --resource-group OCC_ASD_Ayoub --lb-name myLoadBalancer
done

echo "Creating public IP address for NAT gateway..."
az network public-ip create --resource-group OCC_ASD_Ayoub --name myNATgatewayIP --sku Standard --zone 1 2 3

echo "Creating NAT gateway..."
az network nat gateway create --resource-group OCC_ASD_Ayoub --name myNATgateway --public-ip-addresses myNATgatewayIP --idle-timeout 10

echo "Updating subnet configuration to use NAT gateway..."
az network vnet subnet update --resource-group OCC_ASD_Ayoub --vnet-name equipe4_Vnet --name equipe4_Vnet_Subnet_webservers --nat-gateway myNATgateway

echo "Enable the DDoS protection plan"
az network public-ip update \
        --resource-group OCC_ASD_Ayoub \
        --name equipe4_fw_pIP \
        --ddos-protection-mode Enabled


echo "Wordpress configuration"
cd /home/ayoub/monitorwp/Ayoub-loadbalancer/wordpress
ansible-playbook equipe4_configWP.yml

echo "Application Insights plugin installation and activation"
ansible-playbook insights.plugin.yml

sleep 5 

